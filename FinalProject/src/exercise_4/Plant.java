package exercise_4;

import java.util.Random;

public class Plant {
	
	int orders;
	
	public Plant(int orders) {
		this.orders = orders;
	}
	
	public void day() {
		
		Random rnd = new Random();
		
		double possibility = rnd.nextDouble() * 100;
		
		if(possibility >= 0 && possibility < 30) {
			orders += 2;
		}
		if(possibility >= 30 && possibility < 80) {
			orders++;
		}
		if(possibility >= 80 && possibility <= 100) {
			orders += 0;
		}
		
		orders--;
		
	}

	public int getOrders() {
		return orders;
	}

	public void setOrders(int orders) {
		this.orders = orders;
	}
	
	

}
