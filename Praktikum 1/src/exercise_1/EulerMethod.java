package exercise_1;

public class EulerMethod {
	
	private static double[] exact;
	private static double[] approx;
	
	private static double eps = 1E-9;
	
	public static void main (String[] args) {
		
		int t = 19;
		
		exact = new double[t];
		approx = new double[t];
		
		exact(t);
		approx(t);
		
		System.out.println("  year  |  exact value  |  approx value  ");
		System.out.println("-----------------------------------------");
		
		for(int i = 0; i < t; i++) {
			
			String ex = String.format("%.3f", exact[i]);
			String app = String.format("%.3f", approx[i]);

			System.out.println(String.format("   %2d   |   %8s $  |   %8s $ ", i+1, ex, app));
			
		}
		
	}
	
	private static void exact(int t) {
		double value_old = 1000.0;
		double delta_t = (double) (20-1)/t;
		int j = 0;
		for(double i = 1.0; j < t; i+=delta_t, j++) {
			
			exact[j] = (value_old / (Math.exp(0.1))) * Math.exp(0.1 * (i));
			
		}
		
	}
	
	private static void approx(int t) {
		
		double delta_t = (double) (20-1)/t;
		double value_old = 1000.0;
		double value_new = 0.0;
		double rate = 0.1;
		
		double error = 0.0;
		double temp1 = 0.0;
		double temp2 = 0.0;
		
		for(int i = 0; i < t; i++) {
			
			if(i == 0) {
				
				approx[i] = value_old;
				
				continue;
				
			}
			
			approx[i] = value_old + (rate * value_old) * delta_t;
			value_old = approx[i];
		}
		
	}

}