package exercise_4;

public class Plants {

	private static Plant plant;

	private static int closed = 0;
	private static int expand = 0;

	public static void main(String[] args) {

		run(10000);

	}

	public static int calcPlants() {

		int dayCount = 0;
		
		plant = new Plant(25);

		while (true) {
			plant.day();
			dayCount++;

			if (plant.getOrders() == 100) {
				expand++;
				return dayCount;
			}

			if (plant.getOrders() == 0) {
				closed++;
				return -dayCount;
			}

		}

	}

	public static void run(int n) {
		
		int avgClose = 0;
		int avgExpand = 0;

		for (int i = 0; i < n; i++) {

			int days = calcPlants();

			if (days <= 0) {
				days *= -1;

				avgClose += days;

			}

			else {
				
				avgExpand += days;

			}

		}
		
		if(closed != 0) {
			avgClose /= closed;

		}
		if(expand != 0) {
			avgExpand /= expand;
		}
		
		double percentExpand = ((double) expand / (double) n) * 100;
		
		System.out.printf("The probability that a new larger plant will be opened is %.2f%s at %d runs!\n", percentExpand, "%", n);
		System.out.printf("An average plant lasts %d days before its closed because there are no more orders and %d days before its closed because a larger one is needed.", avgClose, avgExpand);
		

	}

}
