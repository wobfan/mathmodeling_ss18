package exercise_4;

import exercise_3.TridiagonalSystem;

public class NaturalCubicSpline extends TridiagonalSystem {
	
	private static int n;

	public static void main(String[] args) {
		
		n = 21;
		
		double[] node = new double[n];
		double[] f = new double[n];
		
		node[ 0]=0.9;
		node[ 1]=1.3;
		node[ 2]=1.9;
		node[ 3]=2.1;
		node[ 4]=2.6;
		node[ 5]=3.0;
		node[ 6]=3.9;
		node[ 7]=4.4;
		node[ 8]=4.7;
		node[ 9]=5.0;
		node[10]=6.0;
		node[11]=7.0;
		node[12]=8.0;
		node[13]=9.2;
		node[14]=10.5;
		node[15]=11.3;
		node[16]=11.6;
		node[17]=12.0;
		node[18]=12.6;
		node[19]=13.0;
		node[20]=13.3;
		
		f[ 0]=1.3;
		f[ 1]=1.5;
		f[ 2]=1.85;
		f[ 3]=2.1;
		f[ 4]=2.6;
		f[ 5]=2.7;
		f[ 6]=2.4;
		f[ 7]=2.15;
		f[ 8]=2.05;
		f[ 9]=2.1;
		f[10]=2.25;
		f[11]=2.3;
		f[12]=2.25;
		f[13]=1.95;
		f[14]=1.4;
		f[15]=0.9;
		f[16]=0.7;
		f[17]=0.6;
		f[18]=0.5;
		f[19]=0.4;
		f[20]=0.25;
		
		double[] h = new double[n];
		
		for(int i = 0; i < n; i++) {
			
			if(i == n-1) {
				
				h[i] = 0;
				break;
				
			}
			
			h[i] = node[i+1] - node[i];
			
		}
		
		double[] a = f;
		double[] l = new double[n];
		double[] d = new double[n];
		double[] u = new double[n];
		
		double[] b = new double[n];
		double[] x = new double[n];
		
		for(int i = 0; i < n; i++) {
			if(i == 0) {
				l[i] = 0.0;
				d[i] = 1.0;
				u[i] = 0.0;
				b[i] = 0.0;
				
				continue;
			}
			
			if(i == n-1) {
				
				l[i] = 0.0;
				d[i] = 1.0;
				u[i] = 0.0;
				b[i] = 0.0;
				
				continue;
			}
			
			l[i] = h[i - 1];
			d[i] = 2 * (h[i-1] + h[i]);
			u[i] = h[i];
			b[i] = (3.0/h[i])*(a[i+1]-a[i]) - (3.0/h[i-1])*(a[i]-a[i-1]);			
		}
		
		TridiagonalSystem.tridiag(l, d, u, b, x);
		
		double[] c = x;
		b = new double[n];
		d = new double[n];
		
		for(int i = 0; i < n-1; i++) {
			
			b[i] = (a[i+1]-a[i])/h[i] - (h[i]/3.0)*(2.0*c[i]+c[i+1]);
			d[i] = (c[i+1]-c[i])/(3.0*h[i]);
			
		}
		
		b[n-1] = 0;
		d[n-1] = 0;
		
		System.out.println("   i  |     a[i]     |     b[i]     |     c[i]     |     d[i]     ");
		System.out.println("------------------------------------------------------------------");
		
		for(int i = 0; i < n; i++) {
			
			String aS = String.format("%.6f", a[i]);
			String bS = String.format("%.6f", b[i]);
			String cS = String.format("%.6f", c[i]);
			String dS = String.format("%.6f", d[i]);
			
			System.out.printf("  %2d  |  %10s  |  %10s  |  %10s  |  %10s  \n", i, aS , bS, cS, dS);
		}
		
		System.out.println();
		
		double[] interpolated = interpolateThose(true, node, f, a, b, c, d, 50);
		
		//for (int i = 0; i < interpolated.length; i++) System.out.printf("%d: %f\n", i, interpolated[i]);
		
	}
		
	public static double[] interpolateThose(boolean print, double node[], double f[], double a[], double b[], double c[], double d[], int amount) {
				
		if (print) System.out.printf("\nprinting %d interpolated function values:\n", amount);
		
		double[] values = new double[amount];
		double dX = Math.abs(node[node.length - 1] - node[0]) / (amount-1);
				
		int i = 0;
		for (double j = node[0]; i < amount; j+=dX, i++) {
			values[i] = getValue(node, f, a, b, c, d, j);
			if (print) System.out.printf("f(%2.1f)\t= %f\n", j, values[i]);
		}
		
		return values;
	}
	
	public static double getValue(double node[], double f[], double a[], double b[], double c[], double d[], double x) {
		final double EPS = 1.E-8;
		if (x < node[0]) return 0.0;
		else if (x > node[node.length - 1] && (x < node[node.length - 1] + EPS || x > node[node.length - 1] - EPS)) return f[f.length - 1];		// machine error handling
		else if (x > node[node.length - 1]) return 0;
		
		int i = 0;
		while (i < node.length) {
			if (x < node[i+1]) break;
			i++;
		}
		double x1 = x - node[i];
		double y = a[i] + b[i] * x1 + c[i] * Math.pow(x1, 2) + d[i] * Math.pow(x1, 3);
		
		return y;
	}


}
