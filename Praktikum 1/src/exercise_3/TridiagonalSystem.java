package exercise_3;

public class TridiagonalSystem {
	
	private static int n = 0;

	public static void main(String[] args) {
		
		n = 4;

		double[] l = new double[n];
		double[] d = new double[n];
		double[] u = new double[n];
		
		double[] b = new double[n];
		double[] x = new double[n];
		
		d[0]= 1; d[1]= 2; d[2]= 2; d[3]=-2;
		u[0]=-1; u[1]=-1; u[2]= 1; u[3]= 0;
		l[0]= 0; l[1]=-1; l[2]= 1; l[3]= 1;
		b[0]=-1; b[1]= 6; b[2]=12; b[3]=-5;
		
		tridiag(l, d, u, b, x);
		
		for(int i = 0; i < n; i++) {
			
			String sol = String.format("x%d = %.3f", i, x[i]);
			System.out.println(sol);
			
		}
		
		System.out.println();
		
		n = 7;
		
		l = new double[n];
		d = new double[n];
		u = new double[n];
		
		b = new double[n];
		x = new double[n];
		
		d[0]= 2; d[1]= 4; d[2]= 8; d[3]=10; d[4]= 7; d[5]= 5; d[6]= 3;
		u[0]= 1; u[1]= 2; u[2]= 4; u[3]= 2; u[4]=-1; u[5]= 1; u[6]= 0;
		l[0]= 0; l[1]= 1; l[2]=-3; l[3]=-1; l[4]= 2; l[5]= 2; l[6]= 2;
		b[0]=16; b[1]=30; b[2]=48; b[3]=74; b[4]=38; b[5]=32; b[6]=20;
		
		tridiag(l, d, u, b, x);
		
		for(int i = 0; i < n; i++) {
			
			String sol = String.format("x%d = %.3f", i, x[i]);
			System.out.println(sol);
			
		}
		
		
		
	}
	
	public static void tridiag (double l[], double d[], double u[], double b[], double x[]) {
		
		int n = x.length - 1;
		
		double y[] = new double[n + 1];

		u[0] = u[0] / d[0];
		y[0] = b[0] / d[0];

		for (int i = 1; i <= n - 1; i++) {
			
			// factoring
			d[i] = d[i] - l[i] * u[i - 1];
			u[i] = u[i] / d[i];
			// forward solve
			y[i] = (b[i] - l[i] * y[i - 1]) / d[i];
			
		}

		d[n] = d[n] - l[n] * u[n - 1];
		y[n] = (b[n] - l[n] * y[n - 1]) / d[n];

		// backsolve
		x[n] = y[n];

		for (int i = n - 1; i >= 0; i--) {
			
			x[i] = y[i] - u[i] * x[i + 1];
			
		}
		
	}

}
