package exercise_5;

public class MonteCarloMethod {

	public static void main(String[] args) {

		System.out.printf("Trials: %d\n    ->        Pi = %.2f\n    -> Approx Pi = %.2f", 100, Math.PI, calcMonteCarlo(100));

		System.out.printf("\n\nTrials: %d\n    ->        Pi = %.3f\n    -> Approx Pi = %.3f", 1000, Math.PI, calcMonteCarlo(1000));

		System.out.printf("\n\nTrials: %d\n    ->        Pi = %.4f\n    -> Approx Pi = %.4f", 10000, Math.PI, calcMonteCarlo(10000));

		System.out.printf("\n\nTrials: %d\n    ->        Pi = %.5f\n    -> Approx Pi = %.5f", 100000, Math.PI, calcMonteCarlo(100000));

		System.out.printf("\n\nTrials: %d\n    ->        Pi = %.6f\n    -> Approx Pi = %.6f", 1000000, Math.PI, calcMonteCarlo(1000000));

	}

	public static double calcMonteCarlo(int n) {

		double res = 0.0;

		int count_under_circle = 0;

		for (int i = 0; i < n; i++) {

			double x = Math.random();
			double y = Math.random();

			double point = Math.sqrt((x * x) + (y * y));

			if (point <= 1) {

				count_under_circle++;

			}

		}

		res = 4 * (count_under_circle / (double)n);

		return res;

	}

}
