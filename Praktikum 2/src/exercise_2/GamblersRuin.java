package exercise_2;

public class GamblersRuin {
	
	private static int stock = 0;

	public static void main(String[] args) {
		
		double win = (double) 1/3;
		double lose = (double) 2/3;
		
		stock = 10;
		
		double[] chance_to_lose = gamble(win, lose);
		
		for(int i = 0; i < chance_to_lose.length; i++) {
			
			System.out.printf("Chance to lose all at q[%d]: %.3f%s\n", i, chance_to_lose[i]*100, "%");
			
		}
				
	}
	
	public static double[] gamble(double win, double lose) {
		
		double[] ret = new double[stock + 1];
		
		double a = (-1) * Math.pow(2,stock) / (1 - (Math.pow(2,stock)));
		double b = 1 - a;
		
		for(int i = 0; i <= stock; i++) {
			
			ret[i] = a + (b * Math.pow(2, i));
			
		}
		
		for(int i = 0; i <= stock; i++) {
			
			if(i == 0) {
				
				ret[i] = 1;
				continue;
				
			}
			
			if(i == stock) {
				
				ret[i] = 0;
				continue;
				
			}
			
			ret[i] = (lose * ret[i - 1]) + (win * ret[i + 1]);
			
		}
		
		return ret;
		
	}

}
