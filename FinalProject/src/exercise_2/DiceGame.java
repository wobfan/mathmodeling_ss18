package exercise_2;

import java.util.Random;

public class DiceGame {

	public static void main(String[] args) {

		dice(10000);
		dice(100000);
		dice(1000000);
	}
	
	public static void dice(int n) {
		
		int diceAttack1;
		int diceAttack2;
		int diceDefend;
		
		int diceAttack;
		
		int winCount = 0;
		
		Random rand = new Random();
		
		for(int i = 0; i <= n; i++) {
			
			diceAttack1 = rand.nextInt(6) + 1;
			diceAttack2 = rand.nextInt(6) + 1;
			diceDefend = rand.nextInt(6) + 1;
			
			diceAttack = diceAttack1 + diceAttack2;
			
			if(diceAttack > diceDefend) {
				
				winCount++;
				
			}
			
		}
		
		double percentageWin = (winCount / (double) n) * 100;
		
		System.out.printf("The Attacker won %.2f%s out ouf %d rolls\n", percentageWin, "%", n);
				
	}

}
